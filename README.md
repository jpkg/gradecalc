# Gradecalc

Calculates grade averages through a TOML file (perl rewrite)

# Installation

You must install the TOML module from cpan in order for the script to work.

```sh
git clone https://git.junickim.me/gradecalc.git
cd gradecalc/env
./build.pl
```

You will be given the option to append the resulting env.sh file to your shell
configuration file (if your shell is supported).

# Writing the toml File

Each table represents a category, and key-value pairs correspond to the names
and scores of each assignment/grade. The "weight" key is reserved for the
weight of the grade (in percent).

## Types of scores

gradecalc accepts two different types of score types in a configuration file.

To represent your grade as a percent, simply write "[percentage]%". The "%" can
either be placed or not. This will evaluate to percentage/100.

To represent your grade in terms of points, use "\[points earned\]/\[points
received\]" or "\[points earned\]:\[points received\]".

Note that **you cannot use both formats in the same category, and gradecalc
will enforce this.**

## Example

```toml
[ Category ]
weight = 30 # counts for 30% of grade.
assignment1 = "30/40" # counts 30/40.
assignment2 = "40/40" # 40/40 points.

[ Category2 ]
weight = 50 # sum of all weight does not necessarily have to be 100.
assignment1 = "90"
assignment2 = "95"
```
