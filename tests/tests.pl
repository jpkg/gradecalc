#!/usr/bin/env perl

use strict;
use warnings;
use diagnostics;

# import from lib.
use File::Spec::Functions;
use File::Basename;
use lib catfile (dirname(__FILE__), "..", "lib");
use Score;

my $a = {
    "WA" => {
        "weight" => "30",
        "x" => "40/50",
        "y" => "50/50"
    },
    "Test" => {
        "weight" => "70",
        "test" => "100"
    }
};

my $b = {
    "WA" => {
        "weight" => "30",
        "x" => "40/50",
        "y" => "30/50"
    },
    "Test" => {
        "weight" => "70",
        "test" => "80"
    }
};

my $c = {
    "WA" => {
        "noweight" => "30",
        "x" => "40/50",
        "y" => "30/50"
    },
    "Test" => {
        "weight" => "70",
        "test" => "80"
    }
};

my $d = {
    "WA" => {
        "noweight" => "30",
        "x" => "40/50",
        "y" => "30/50"
    },
    "Test" => {
        "noweight" => "70",
        "test" => "80"
    }
};



Score::parse $a,1;
print"\n";
Score::parse $b,1;
print"\n";
Score::parse $c,1;
print"\n";
Score::parse $d,1;
