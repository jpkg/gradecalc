use warnings;
use diagnostics;
use strict;

package Message;

sub error {
    my ($msg) = @_;
    return "\e[1;31merror:\e[0m $msg\n";
}

sub warn {
    my ($msg) = @_;
    return "\e[1;33mwarning:\e[0m $msg\n";
}

1;
