#!/usr/bin/env perl

package Score;

use File::Basename;
use lib dirname(__FILE__);
use Message;
use Data::Dumper;

# run a regex for grade type.
sub gradetype {
    my ($score) = @_;
    if ($score =~ /^\s*[0-9]+\s*(\/|:)\s*[0-9]+\s*$/) {
        # score
        my $s = $score =~ s/\s//r;
        my ($earned, $possible) = split(/[\/,:]+/, $s);
        return [$earned, $possible];
    } elsif ($score =~ /^[0-9]+\s*%?$/) {
        my $percent = $score =~ s/(\s|%)//r;
        return [$percent];
    } else {
        return [];
    }
}

# calculates sum and weight with hash, name and whether or not it is a test.
sub category {
    my ($d, $name, $test) = @_;
    my $wt = $d->{'weight'} or 
        do {
            print(Message::error("No key 'weight' in category '$name'."));
            if (! $test) {
                exit 1;
            } else {
                return ();
            }
        };
    delete ($d->{'weight'});
    my ($pts, $pct) = ([], []);
    foreach my $v (values %$d) {
        my $t = gradetype ($v);
        if (scalar ($t->@*) == 2) {
            push ($pts->@*, $t);
        } elsif (scalar ($t->@*) == 1) {
            push $pct->@*, $t;
        } else {
            print(Message::warn("'$v' unparsable."));
        }
    }
    if (scalar($pts->@*) && scalar($pct->@*)) {
        print (Message::error("Category '$name' appears to have both percentages and points systems. You may only use one of the two."));
        if (! $test) {
            exit 1;
        } else {
            return;
        }
    } elsif (scalar ($pts->@*)) {
        my ($e, $p) = (0, 0);
        foreach my $x ($pts->@*) {
            $e += $x->[0];
            $p += $x->[1];
        }
        return ($e/$p * 100, $wt);
    } elsif (scalar($pct->@*)) {
        my $len = scalar ($pct->@*);
        my $sum = 0;
        foreach my $x ($pct->@*) {
            $sum += $x->[0]/100;
        }
        return ($sum/$len * 100, $wt);
    } else {
        return ();
    }
}

my $grades = {
    92.5 => "\e[1;32mA\e[0m",
    89.5 => "\e[1;33mA-\e[0m",
    86.5 => "\e[1;31mB+\e[0m",
    82.5 => "\e[1;31mB\e[0m",
    79.5 => "\e[1;31mB-\e[0m",
};

sub mime_avg {
    my ($score) = @_;
    my $letter = "\e[1;31mUnacceptable\e[0m";
    my $maxv = 0;
    foreach my $k (keys $grades->%*) {
        if ($score >= $k && $k >= $maxv) {
            $letter = $grades->{$k};
            $maxv = $k;
        }
    }
    return $letter;
}

sub parse {
    my ($d, $test) = @_;
    my $wts = 0;
    my $sum = 0;
    foreach my $k (keys $d->%*) {
        my @x = category ($d->{$k}, $k, $test);
        if (@x) {
            my ($score, $wt) = @x;
            $sum += $score * $wt;
            $wts += $wt;
            print "Category '$k', Weight: $wt, Score: $score/100\n" ;
        }
    }
    if ($sum == 0 && $wts == 0) {
        print Message::error "no average calculated.";
        if (! $test) {
            exit 1;
        } else {
            return;
        }
    }
    my $letter = mime_avg($sum/$wts);
    print "Final Average: " . $sum/$wts . " ($letter)". "\n";
}




1;
